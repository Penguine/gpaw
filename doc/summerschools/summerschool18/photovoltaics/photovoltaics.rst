===============
 Photovoltaics
===============

In this exercise we study some of the key properties of materials for
photovoltaic applications. We will investigate the

* atomic structure (creation/optimization),

* band gap, band gap position, and band structure,

* comparison of different exchange correlation functionals perform,

* and evaluate the absorbtion (excluding excitonic effects).

:download:`pv1.ipynb`, :download:`pv2.ipynb`, :download:`pv3.ipynb`
